---
version: 2
titre: Amélioration de la plateforme d’analyse du COVID-19 via Twitter
type de projet: Projet de semestre 5
année scolaire: 2020/2021
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Lucie Tornay
  - Simon Ruffieux
  - Quentin Meteier
mots-clé: [Web interface, Machine learning, Text analysis, Web app]
langue: [F]
confidentialité: non
suite: non
---
## Contexte

« Covid Twitter Activity Analysis » est une plateforme développée en 2020 par l’institut HumanTech pour l’OFSP. La plateforme a été développée avec les frameworks Vue.js et Django. Le but de cette plateforme est de surveiller l'activité de Twitter pour détecter les tweets liés à la pandémie COVID-19. Les données recueillies sont ensuite traitées par un algorithme afin d’identifier les tweets mentionnant un cas positif. Ces données permettent à l’OFSP de suivre la propagation et l'évolution de cette pandémie en Suisse au travers de réseaux sociaux. 

Le but du projet de semestre est d’ajouter une fonctionnalité à cette plateforme afin de supporter la gestion de plusieurs types d’algorithme de traitement des données. Il faut permettre de choisir l’algorithme de Machine Learning à entraîner et à utiliser directement au travers de l’interface. Le but du projet consiste également à implémenter un nouvel algorithme (simple) permettant de classifier automatiquement si le tweet doit être considéré comme positif ou non, en utilisant la librairie Python Scikit-learn.

## Objectifs

- Modifier l’interface (et le backend) afin de supporter plusieurs algorithmes différents
- Analyser les algorithmes de Machine Learning performants pour l’analyse de données textuelles 
- Implémenter pour la plateforme au moins un des algorithmes retenus grâce à la librairie Python Scikit-learn 

## Contraintes

- Utiliser la plateforme web déjà existante


